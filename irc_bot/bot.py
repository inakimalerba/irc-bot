import flask

from . import flask_queue
from . import handlers

bp = flask.Blueprint('bot', __name__)


@bp.route('/', methods=['GET'])
def index():
    return flask.Response(response='OK', status='200')


@bp.route('/', methods=['POST'])
def webhook_receiver():
    webhook_json = flask.request.get_json()

    # Determine the event type and run a handler that matches.
    event_type = webhook_json['object_kind']
    handler = getattr(handlers, 'handle_{}'.format(event_type))
    irc_message = handler(webhook_json)

    # Put a message on the queue for IRC delivery.
    if irc_message:
        queue = flask_queue.get_queue()
        for line in irc_message:
            queue.put(line)

    return flask.Response(response='OK', status='200')


@bp.route('/message', methods=['POST'])
def message_receiver():
    irc_json = flask.request.get_json()
    message = irc_json['message']
    flask_queue.get_queue().put(message)
    return flask.Response(response='OK', status='200')


@bp.route('/sentry', methods=['POST'])
def sentry_receiver():
    irc_json = flask.request.get_json()
    project_name = irc_json['project_name']
    message = irc_json['message']
    url = handlers.try_shorten_url(irc_json['url'])
    message = f'😩 {project_name}: {message} - {url}'
    flask_queue.get_queue().put(message)
    return flask.Response(response='OK', status='200')
