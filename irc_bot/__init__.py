"""Main script to start the Flask app."""
import logging
import sys

from flask import Flask, g

from irc_bot import bot

logging.basicConfig(
    format='%(asctime)s %(levelname)s %(message)s',
    level=logging.DEBUG,
    stream=sys.stdout
)

def create_app(test_config=None):
    """Create and configure an instance of the Flask application."""
    app = Flask(__name__, instance_relative_config=True)
    app.register_blueprint(bot.bp)

    return app

