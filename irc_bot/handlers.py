import os

from collections import Counter
from distutils.util import strtobool
import ircmessage as ircm
from datetime import datetime, timedelta
from dateutil.parser import parse as date_parse
import requests


def colorize_status(status):
    if status == 'SUCCESS':
        color = 'lime'
    elif status == 'FAILED':
        color = 'red'
    else:
        color = 'silver'

    return ircm.style(status, fg=color, bold=True)


def last_stage_run(jobs):
    return [x['stage'] for x in jobs[::-1] if x['status'] != 'skipped'][0]


def try_shorten_url(url):
    """
    Attempt to shorten a URL using Red Hat's internal URL-shortening service.

    Args:
        url:    The URL to shorten
    Returns:
        The shortened URL, if succeeded.
        The original URL, if not.
    """
    payload = {
        'url': url,
    }

    try:
        r = requests.post('https://url.corp.redhat.com/new', data=payload)
        if r.status_code == 200:
            return r.text.strip()
    except requests.exceptions.ConnectionError:
        pass
    except requests.exceptions.SSLError:
        pass
    return url


def handle_pipeline(hook_json):
    pipeline_id = hook_json['object_attributes']['id']
    pipeline_status = hook_json['object_attributes']['status']

    # If the pipeline is in a pending state, don't say anything in IRC.
    if pipeline_status == 'pending':
        return None

    trigger_vars = {
        x['key']: x['value'] for x in
        hook_json['object_attributes']['variables']
    }

    # Before doing anything, verify this is actually a repo and branch we want
    # to process.
    allowed_projects = {}
    allowed_projects_string = os.environ.get('PROJECTS')
    # An `if` is easier than trying to default to empty strings and then
    # working around those as split() on empty string returns ['']...
    if allowed_projects_string:
        for repo_branch in allowed_projects_string.split(','):
            repo, branch = repo_branch.split(':')
            allowed_projects.setdefault(repo, set())
            allowed_projects[repo].add(branch)

    # Are we filtering this repo?
    repo = trigger_vars['cki_project']
    if repo in allowed_projects:
        if trigger_vars['cki_pipeline_branch'] not in allowed_projects[repo]:
            return

    msg_prefix = ircm.style(f"#{pipeline_id}:", fg='blue')

    msg = f"{msg_prefix} {colorize_status(pipeline_status.upper())}"

    stages = hook_json['object_attributes']['stages']
    stage_jobs = {}
    stage_messages = []
    for stage in stages:
        if stage == 'review':
            continue

        jobs_in_stage = [
            x for x in hook_json['builds'] if x['stage'] == stage
            and x['status'] != 'skipped'
        ]
        sorted_jobs_in_stage = sorted(
            jobs_in_stage,
            reverse=True,
            key=lambda k: date_parse(k['created_at'])
        )
        stage_jobs[stage] = []
        for job in sorted_jobs_in_stage:
            if job['name'] not in [x['name'] for x in stage_jobs[stage]]:
                stage_jobs[stage].append(job)

        stage_count = Counter([x['status'] for x in stage_jobs[stage]])

        if stage_count['failed'] > 0:
            # At least one job failed in the stage.
            color = 'red'
        elif stage_count['running'] > 0:
            # At least one job in the stage is still running.
            color = 'silver'
        elif stage_count['skipped'] == len(stage_jobs[stage]):
            # All of the jobs in this stage were skipped!
            color = 'black'
        elif stage_count['success'] == len(stage_jobs[stage]):
            # All of the jobs passed.
            color = 'lime'
        else:
            color = None

        if color:
            stage_messages.append(ircm.style(stage[0].upper(), fg=color))
        else:
            stage_messages.append(stage[0].upper())

    msg += f" [{''.join(stage_messages)}]"

    # Skip retriggers
    if strtobool(trigger_vars.get('retrigger', 'false')):
        return []

    # If the pipeline duration is set, send that to IRC.
    if 'duration' in hook_json['object_attributes']:
        if hook_json['object_attributes']['duration']:
            duration = int(hook_json['object_attributes']['duration'])
            pretty_duration = str(timedelta(seconds=duration))
            msg += f" [{pretty_duration}]"

    # Add on extra trigger variables
    msg += f" {trigger_vars['title']} "

    # Add a shortened link to the pipeline page.
    project_baseurl = hook_json['project']['web_url']
    pipeline_url = f"{project_baseurl}/pipelines/{pipeline_id}"

    # Do not print internal URLs unless specified for internal bots. The URLs
    # themselves are fine to be public as they don't contain any confidential
    # info but they will confuse people because they can only be opened when
    # running in Red Hat network.
    print_urls = os.environ.get('PRINT_URLS', 'False')
    if print_urls == 'True':
        msg += f" {try_shorten_url(pipeline_url)}"

        # Add a shortened link to the osci dashboard
        if trigger_vars['cki_pipeline_branch'] == 'fedora':
            # Skip fedora, it isn't part of OSCI
            pass
        elif pipeline_status != 'running' and 'brew_task_id' in trigger_vars:
            host = f'https://dashboard.osci.redhat.com'
            task_id = trigger_vars['brew_task_id']
            osci_dash = f'{host}/#/artifact/brew-build/aid/{task_id}'
            msg += f" - osci dash {try_shorten_url(osci_dash)}"

    return msg.split('\n')
