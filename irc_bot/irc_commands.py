import datetime
import json
import os
import random
import re
import traceback

import gitlab
import requests
from ircmessage import style

from .cki_jira import Jira
from .handlers import try_shorten_url


JIRA_SERVER = os.environ['JIRA_SERVER']
JIRA_USERNAME = os.environ['JIRA_USERNAME']
JIRA_PASSWORD = os.environ['JIRA_PASSWORD']
JIRA_CLIENT = Jira(JIRA_SERVER, JIRA_USERNAME, JIRA_PASSWORD)
GITLAB_PRIVATE_TOKEN = os.environ['GITLAB_PRIVATE_TOKEN']
GITLAB_SNIPPET = os.environ['GITLAB_SNIPPET']
GITLAB_INSTANCES = json.loads(os.environ.get('GITLAB_INSTANCES', '{}'))
OK_EMOJIS = ['👌', '🆗', '🎉', '💃', '😊', '😁', '🥰', '😘', '🤗', '🤭']
TEAM_MEMBERS = os.environ.get('TEAM_MEMBERS', '')
STANDUPS = json.loads(os.environ.get('STANDUPS', '{}'))


class SearchIssue():
    name = 'search'
    usage = 'Search jira issues - usage: "search pattern" more info https://confluence.atlassian.com/jiracoreserver073/advanced-searching-861257209.html'
    nargs = 'n'

    def run(self, conn, event, args):
        if not args:
            conn.privmsg(event.target, f'{self.usage}')
            return
        pattern = ' '.join(args)
        if 'status' not in pattern:
            pattern += ' AND status!=Done'
        for idx, issue in enumerate(JIRA_CLIENT.search_issues(pattern)):
            conn.privmsg(
                event.target, f' {issue.key} - {issue.fields.summary}')
            if idx >= 5:
                break


class CreateIssueBase():
    name = 'create'
    usage = 'Create a jira issue - usage: "new summary. description: here the description"'
    nargs = 'n'

    def run(self, conn, event, args):
        try:
            summary, description = ' '.join(args).split('description:')
        except ValueError:
            conn.privmsg(
                event.target, f'"description: " is required. {self.usage}')
            return

        nickname = event.source.split('!')[0]
        i_type = 'Bug' if self.name == 'new-bug' else 'Task'
        issue = JIRA_CLIENT.create_issue(summary.strip(), description.strip(),
                                         nickname, i_type=i_type)
        conn.privmsg(
            event.target, f'{random.choice(OK_EMOJIS)} {issue.key} created')


class NewTask(CreateIssueBase):
    name = 'new-task'


class NewBug(CreateIssueBase):
    name = 'new-bug'


class Super():
    name = 'super'
    usage = 'Run a combination of other irc-bot commands'
    nargs = 'n'

    def __init__(self, lst):
        self.lst = lst

    def run(self, conn, event, args):
        if not args:
            conn.privmsg(event.target, f'{self.usage}')
            return

        # arguments are already split, join them to split them
        # in a different manner
        cmd = ' '.join(args)

        # +! delimits each command in command chain
        for cmd_args in [args.split() for args in cmd.split('+!')]:
            for cmd in self.lst:
                if cmd.name == cmd_args[0]:
                    # run one of the commands in chain
                    cmd.run(conn, event, cmd_args)
                    break
            else:
                # one of the commands in cmd_args wasn't found...
                conn.privmsg(event.target, f'Cannot find command'
                                           f' "{cmd_args[0]}". {self.usage}')
                return

        conn.privmsg(
            event.target, f'{random.choice(OK_EMOJIS)} super cmd done!')


class SetState():
    name = 'state'
    usage = 'Set new state to issue - usage: issue In Progress'
    nargs = 'n'

    def run(self, conn, event, args):
        if not args or len(args) < 2:
            conn.privmsg(event.target, f'{self.usage}')
            return
        issue = args[0]
        state = ' '.join(args[1:])
        JIRA_CLIENT.set_state(issue, state)
        conn.privmsg(
            event.target, f'{random.choice(OK_EMOJIS)} new state {state} set to {issue}')


class GetBeakerQueues():
    name = 'queues'
    usage = 'Get the current Beaker queues'
    nargs = 0

    def get_queued_jobs(self, arch, beaker_url, beaker_user):
        # Get the queued jobs for each arch.
        url = f"{beaker_url}/top-recipe-owners.{arch}"
        resp = requests.get(url, stream=True)
        for line in resp.text.split('\n'):
            if line.startswith(beaker_user):
                return line.split()[1]
        return 0

    def run(self, conn, event, args):
        # Get the Beaker URL and error out if it's not set.
        beaker_url = os.environ.get('BEAKER_STATS_URL', None)
        if not beaker_url:
            conn.privmsg(
                event.target,
                f'👎 Somebody forgot to set BEAKER_STATS_URL'
            )
            return False

        beaker_user = os.environ.get('BEAKER_USER', None)
        if not beaker_user:
            conn.privmsg(
                event.target,
                f'👎 Somebody forgot to set BEAKER_USER'
            )
            return False

        beaker_host = os.environ.get('BEAKER_HOST', None)
        if not beaker_host:
            conn.privmsg(
                event.target,
                f'👎 Somebody forgot to set BEAKER_HOST'
            )
            return False

        # Get the total jobs running.
        params = {
            "jobsearch-0.table": "Group",
            "jobsearch-0.operation": "is",
            "jobsearch-0.value": "cki",
            "jobsearch-1.table": "Status",
            "jobsearch-1.operation": "is not",
            "jobsearch-1.value": "Aborted",
            "jobsearch-2.table": "Status",
            "jobsearch-2.operation": "is not",
            "jobsearch-2.value": "Cancelled",
            "jobsearch-3.table": "Status",
            "jobsearch-3.operation": "is not",
            "jobsearch-3.value": "Completed",
        }
        url = f"https://{beaker_host}/jobs/"
        resp = requests.get(url, params=params)
        running_jobs = re.findall(r"Items found: (\d*)", resp.text)[0]

        # Get queues for each architecture.
        arches = ['aarch64', 'ppc64le', 's390x', 's390x_z13', 'x86_64']
        for idx, arch in enumerate(arches):
            result = self.get_queued_jobs(arch, beaker_url, beaker_user)
            arches[idx] = f"{arch}: {result}"

        irc_message = (
            f"🤖 CKI Beaker jobs running: {running_jobs}; "
            f"Queues: {', '.join(arches)}"
        )

        conn.privmsg(event.target, irc_message)


class CommentIssue():
    name = 'comment'
    usage = 'Add a comment on a jira issue - usage: "issue some update"'
    nargs = 'n'

    def run(self, conn, event, args):
        if not args:
            conn.privmsg(event.target, f'{self.usage}')
            return
        issue = args[0]
        message = ' '.join(args[1:])
        if not message:
            conn.privmsg(event.target, f'{self.usage}')
            return
        JIRA_CLIENT.add_comment_to_issue(issue, message)
        conn.privmsg(
            event.target, f'{random.choice(OK_EMOJIS)} comment added to {issue}')


class DadJoke():
    name = 'dadjoke'
    usage = 'Get a good Dad joke'
    nargs = 0

    def run(self, conn, event, args):
        headers = {'Accept': 'application/json'}
        resp = requests.get("https://icanhazdadjoke.com/", headers=headers)
        dad_joke = resp.json()['joke'].replace('\n', ' ')
        conn.privmsg(event.target, f'🤠 {dad_joke}')


class SetEstimate():
    name = 'set-estimate'
    usage = 'set estimate for jira issue - usage "estimate" (e.g. 5)'
    nargs = 2

    def run(self, conn, event, args):
        if not args or len(args) < 2:
            conn.privmsg(event.target, f'{self.usage}')
            return

        issue_id = args[0]
        newEstimate = args[1]

        JIRA_CLIENT.set_estimate(issue_id, newEstimate)
        conn.privmsg(event.target,
                     f'{random.choice(OK_EMOJIS)} estimate of {newEstimate} set to issue {issue_id}')


class AssignIssue():
    name = 'assign'
    usage = 're-assign a jira issue - usage: "issue user"'
    nargs = 2

    def run(self, conn, event, args):
        issue, user = args
        JIRA_CLIENT.assign_issue(issue, user)
        conn.privmsg(
            event.target, f'{random.choice(OK_EMOJIS)} {issue} assigned to {user}')


class ListCommands():
    name = 'list'
    usage = 'list'
    nargs = 0

    def run(self, conn, event, args):
        conn.privmsg(event.target, ', '.join(event.cmd_manager.cmds))


class SuccessBot():
    name = "success"
    usage = "talk about some success we had!"
    nargs = 'n'

    def run(self, conn, event, args):
        gl = gitlab.Gitlab(
            'https://gitlab.cee.redhat.com',
            private_token=GITLAB_PRIVATE_TOKEN
        )
        # Get the current snippet content.
        snippet = gl.snippets.get(GITLAB_SNIPPET)
        old_content = snippet.content().decode('utf-8')

        # Add new content.
        nickname = event.source.split('!')[0]
        new_success_message = ' '.join(args)
        timestamp = datetime.datetime.now().isoformat(sep=' ', timespec='seconds')
        new_content = (
            f"* **{nickname}:** {new_success_message} "
            f"*{timestamp}* \n{old_content}"
        )

        # Update the snippet.
        snippet.content = new_content
        snippet.save()

        # Note the success
        conn.privmsg(
            event.target,
            f"{random.choice(OK_EMOJIS)} success noted! "
            f"https://gitlab.cee.redhat.com/snippets/{GITLAB_SNIPPET}"
        )


class CommandManager():
    def __init__(self):
        self.cmds = {}
        self.register_cmd(ListCommands())

    def register_cmd(self, cmd):
        self.cmds[cmd.name] = cmd

    def __call__(self, conn, event):
        text = event.arguments[0].strip()
        if not text.startswith(conn.ircname):
            return
        args = text.split()

        # Mentioning the bot without any arguments causes it to throw an
        # exception and abruptly quit.
        if len(args) < 2:
            return

        cmd_name = args[1]
        if cmd_name not in self.cmds:
            msg = f'Command not found ¯\_(ツ)_/¯, type "{conn.ircname}, list" to get the list of commands available'
            conn.privmsg(event.target, msg)
            return

        self.process_command(cmd_name, args[2:], conn, event)

    def process_command(self, cmd_name, args, conn, event):
        event.cmd_manager = self
        cmd = self.cmds[cmd_name]
        if cmd.nargs != 'n' and cmd.nargs != len(args):
            conn.privmsg(event.target, f'{cmd.usage}')
            return
        try:
            cmd.run(conn, event, args)
        except Exception as e:
            conn.privmsg(event.target, '😢, ups: {!r}'.format(e))


class StandUp():
    name = 'standup'
    usage = 'Get standup notification + random order!'
    nargs = 0

    def run(self, conn, event, args):
        members = TEAM_MEMBERS.split()
        random.shuffle(members)
        weekday = str(datetime.datetime.today().weekday())
        title, url = STANDUPS.get(weekday, ['StandUp', None])
        message = f'🙋 {title}: ' + ', '.join(members)
        if url:
            message += f' - {try_shorten_url(url)}'
        conn.privmsg(event.target, message)


class GitLabMergeRequest():
    """Print information about GitLab merge request URLs."""

    def __init__(self):
        """Init."""
        self.gitlab_instances = {h: gitlab.Gitlab(f'https://{h}', os.environ[t])
                                 for h, t in GITLAB_INSTANCES.items()}

    def __call__(self, conn, event):
        """Process an IRC message."""
        text = event.arguments[0].strip()
        url_pattern = r'https://([^/]+)/(cki-project/[^ ]*)/merge_requests/(\d+)'
        for match in re.findall(url_pattern, text):
            try:
                self.process_merge_request(conn, event, *match)
            except Exception:
                traceback.print_exc()

        project_pattern = r'([^\s]+)!(\d+)'
        for match in re.findall(project_pattern, text):
            try:
                self.process_merge_request(conn, event, None, *match)
            except Exception:
                traceback.print_exc()

    def _search_gitlab_project(self, project, merge_request):
        """Search through known Gitlab instances for the project."""
        if '/' not in project:
            project = f'cki-project/{project}'
        results = []
        for instance in self.gitlab_instances.values():
            try:
                gl_project = instance.projects.get(project)
                if gl_project.archived:
                    continue
                mr = gl_project.mergerequests.get(int(merge_request))
                results.append((gl_project, mr))
            except gitlab.GitlabGetError:
                continue

        return results

    def process_merge_request(self, conn, event, host, project, merge_request):
        """Print information about a GitLab merge request."""
        print(f'Processing GitLab MR {host} {project} {merge_request}')
        if host and host not in self.gitlab_instances:
            print(f'Gitlab instance not supported {host}')
            return

        if host:
            gl = self.gitlab_instances.get(host)
            gl_project = gl.projects.get(re.sub('/-$', '', project))
            results = [(
                gl_project, gl_project.mergerequests.get(int(merge_request))
            )]
        else:
            results = self._search_gitlab_project(project, merge_request)

        for gl_project, mr in results:
            msg = self.generate_message(gl_project, mr)
            conn.privmsg(event.target, msg)

    @staticmethod
    def generate_message(gl_project, mr):
        """Generate message to reply."""
        changes = mr.changes()
        files = changes['changes_count']
        added = sum(len(re.findall(r'^\+', c['diff'], re.M))
                    for c in changes['changes'])
        removed = sum(len(re.findall('^-', c['diff'], re.M))
                      for c in changes['changes'])
        url = try_shorten_url(mr.web_url)

        added = style(f'+{added}', fg='lime')
        removed = style(f'-{removed}', fg='red')
        project_and_mr = style(f'{gl_project.path}!{mr.iid}', bold=True)

        details = f'{mr.author["name"]}, {files}/{added}/{removed} [{mr.state.upper()}]'
        msg = f'{project_and_mr} ({details}): {mr.title} - {url}'
        return msg
