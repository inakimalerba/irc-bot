import jira
from jira.client import GreenHopper


class InvalidIssue(Exception):
    pass


class InvalidUser(Exception):
    pass


class Jira():
    def __init__(self, server, user, password, project='FASTMOVING'):
        options={
            'server': server,
            'mutual_authentication': 'DISABLED',
        }
        self.jira = jira.JIRA(options=options, basic_auth=(user, password))
        self.gh = GreenHopper(options=options, basic_auth=(user, password))
        self.project = project
        self.board = None
        for board in self.gh.boards():
            if board.name.startswith(self.project):
                self.board = board
                break

    def _get_issue(self, issue_id):
        if '-' not in issue_id:
            issue_id = f'{self.project}-{issue_id}'
        try:
            issue = self.jira.issue(issue_id)
        except jira.JIRAError:
            raise InvalidIssue(issue_id)
        return issue

    def get_current_sprint(self):
        for sprint in self.gh.sprints(self.board.id):
            if sprint.state == 'ACTIVE':
                return sprint

    def assign_issue(self, issue_id, user):
        issue = self._get_issue(issue_id)
        if not self.jira.search_users(user):
            raise InvalidUser(user)
        self.jira.assign_issue(issue, user)

    def add_comment_to_issue(self, issue_id, comment):
        issue = self._get_issue(issue_id)
        self.jira.add_comment(issue, comment)

    def search_issues(self, pattern, project=None):
        project = project or self.project
        return self.jira.search_issues(f'project={project} AND {pattern}')

    def set_state(self, issue_id, new_state):
        issue = self._get_issue(issue_id)
        if 'done' in new_state.lower():
            self.add_comment_to_issue(issue_id, 'Marking as done - Yayyy! 💘')
        elif 'in progress' in new_state.lower():
            sprint = self.get_current_sprint()
            if sprint:
                issue.update(fields={'customfield_10005': sprint.id})
        self.jira.transition_issue(issue, new_state)

    def set_estimate(self, issue_id, newEstimate):
        issue = self._get_issue(issue_id)
        issue.update(fields={'customfield_10002': float(newEstimate)})

    def create_issue(self, summary, description, nickname, i_type='Task'):
        description += f'\nCreated by {nickname}'
        issue = self.jira.create_issue(
            fields={
                'summary': summary,
                'description': description,
                'project': self.project,
                'issuetype': i_type,
            },
        )
        return issue
