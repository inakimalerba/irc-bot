from hotqueue import HotQueue

from flask import current_app, g

def get_queue():
    if 'queue' not in g:
        g.queue_conn = HotQueue("webhook-irc", host='localhost')
    return g.queue_conn
