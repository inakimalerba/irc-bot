import logging
import os
import signal
import sys
import time

import irc.client
from hotqueue import HotQueue

from . import irc_commands

logging.basicConfig(
    format='%(asctime)s %(levelname)s %(message)s',
    level=logging.INFO,
    stream=sys.stdout
)


class SignalWatcher:
    """Watch SIGINT/SIGTERM signals to allow graceful termination."""

    should_stop = False

    def __init__(self):
        """Initialize the watcher."""
        signal.signal(signal.SIGINT, self.int_term_handler)
        signal.signal(signal.SIGTERM, self.int_term_handler)

    def int_term_handler(self, signum, frame):
        """Notify that the main loop should terminate."""
        self.should_stop = True


class IrcBot:
    """IRC bot."""

    def __init__(self, irc_server, irc_port, irc_nick, irc_channel):
        """Initialize the IRC bot."""
        self.irc_server = irc_server
        self.irc_port = irc_port
        self.irc_nick = irc_nick
        self.irc_channel = irc_channel

        self.client = irc.client.Reactor()
        self.server = self.client.server()

        self.queue = HotQueue("webhook-irc", host='localhost')
        cmd_manager = irc_commands.CommandManager()
        all_cmds = [irc_commands.SetEstimate(),
                    irc_commands.AssignIssue(),
                    irc_commands.NewTask(),
                    irc_commands.NewBug(),
                    irc_commands.CommentIssue(),
                    irc_commands.DadJoke(),
                    irc_commands.GetBeakerQueues(),
                    irc_commands.SearchIssue(),
                    irc_commands.SetState(),
                    irc_commands.SuccessBot(),
                    irc_commands.StandUp()]
        for cmd in all_cmds:
            cmd_manager.register_cmd(cmd)
        cmd_manager.register_cmd(irc_commands.Super(all_cmds))
        self.client.add_global_handler("pubmsg", cmd_manager)
        self.client.add_global_handler(
            "pubmsg", irc_commands.GitLabMergeRequest())

        self._send_message("Gitlab webhook bot is online!")

    def _send_message(self, message):
        if not self.server.is_connected():
            self.server.connect(self.irc_server, self.irc_port, self.irc_nick)
            self.server.join(self.irc_channel)
        self.server.privmsg(self.irc_channel, message)

    def main(self):
        """IRC bot main loop."""
        try:
            signal_watcher = SignalWatcher()
            while not signal_watcher.should_stop:
                try:
                    message = self.queue.get()

                    if message:
                        self._send_message(self.irc_channel, message)
                        # Send the message to irc at most once per second.
                        time.sleep(1)

                    # Process any IRC events that we need to handle.
                    self.client.process_once(1)
                except Exception:
                    logging.exception('Unable to send message')
                    time.sleep(5)

                # Sleep for 0.5 seconds.
                time.sleep(0.5)

        finally:
            self.server.close()


def main():
    """Start the IRC bot."""
    irc_server = os.environ.get('IRC_SERVER')
    irc_port = int(os.environ.get('IRC_PORT'))
    irc_nick = os.environ.get('IRC_NICK')
    irc_channel = os.environ.get('IRC_CHANNEL')
    IrcBot(irc_server, irc_port, irc_nick, irc_channel).main()
