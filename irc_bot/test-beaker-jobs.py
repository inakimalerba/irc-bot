#!/usr/bin/python3
import re

import requests

params = {
    "jobsearch-0.table": "Group",
    "jobsearch-0.operation": "is",
    "jobsearch-0.value": "cki",
    "jobsearch-1.table": "Status",
    "jobsearch-1.operation": "is not",
    "jobsearch-1.value": "Aborted",
    "jobsearch-2.table": "Status",
    "jobsearch-2.operation": "is not",
    "jobsearch-2.value": "Cancelled",
    "jobsearch-3.table": "Status",
    "jobsearch-3.operation": "is not",
    "jobsearch-3.value": "Completed",
}

url = "https://beaker.engineering.redhat.com/jobs/"
resp = requests.get(url, params=params)
jobs_found = re.findall(r"Items found: (\d*)", resp.text)[0]
