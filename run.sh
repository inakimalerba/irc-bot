#!/bin/bash -x
cd /code

/usr/bin/redis-server /etc/redis.conf &
PID_REDIS="$!"

python3 -m irc_bot &
PID_IRC="$!"

FLASK_APP=irc_bot flask run --host 0.0.0.0 --port 5000 &
PID_FLASK="$!"

# Tell the whole process group that it's time to quit.
trap "trap - SIGTERM && kill -- -$$" SIGINT SIGTERM EXIT

wait
